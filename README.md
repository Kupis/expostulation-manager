# Exspostulation Manager
.NET application to save and open excuses. Now you can easily run from work :).
## Purpose
The purpose of this application is to learn more about file management (updated version use Serialization);
## Author
* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details