﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Expostulation_Manager
{
    public partial class mainForm : Form
    {
        private Excuse currentExcuse = new Excuse();
        private string selectedFolder = "";
        private bool formChanged = false;
        Random random = new Random();

        public mainForm()
        {
            InitializeComponent();
            currentExcuse.LastUseed = lastUsedDatePick.Value;
        }

        private void excuseTextBox_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Description = descriptionTextBox.Text;
            UpdateForm(true);
        }

        private void resultTextBox_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Results = resultTextBox.Text;
            UpdateForm(true);
        }

        private void lastUsedDatePick_ValueChanged(object sender, EventArgs e)
        {
            currentExcuse.LastUseed = lastUsedDatePick.Value;
            UpdateForm(true);
        }

        private void UpdateForm(bool changed)
        {
            if (changed)
                this.Text = "Expostulation Manager*";
            else
            {
                descriptionTextBox.Text = currentExcuse.Description;
                resultTextBox.Text = currentExcuse.Results;
                lastUsedDatePick.Value = currentExcuse.LastUseed;

                if (!String.IsNullOrEmpty(currentExcuse.ExcusePath))
                    fileDateLabel.Text = File.GetLastWriteTime(currentExcuse.ExcusePath).ToString();

                this.Text = "Expostulation Manager";
            }
            formChanged = changed;
        }

        private void directoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.SelectedPath = selectedFolder;
            DialogResult result = folderBrowser.ShowDialog();
            if(result == DialogResult.OK)
            {
                selectedFolder = folderBrowser.SelectedPath;
                saveButton.Enabled = true;
                openButton.Enabled = true;
                randomExcuseButton.Enabled = true;

                descriptionTextBox.Enabled = true;
                resultTextBox.Enabled = true;
                lastUsedDatePick.Enabled = true;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(descriptionTextBox.Text) || String.IsNullOrEmpty(resultTextBox.Text))
            {
                MessageBox.Show("Determine an excuse and result", "The file can not be saved", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = selectedFolder;
            fileDialog.Filter = "Excuse files (*.excuse)|*.excuse|All files (*.*)|*.*";
            fileDialog.FileName = descriptionTextBox.Text + ".excuse";
            DialogResult result = fileDialog.ShowDialog();
            if(result == DialogResult.OK)
            {
                currentExcuse.Save(fileDialog.FileName);
                UpdateForm(false);
                MessageBox.Show("Excuse saved");
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                try
                {
                    OpenFileDialog fileDialog = new OpenFileDialog();
                    fileDialog.InitialDirectory = selectedFolder;
                    fileDialog.Filter = "Excuse files (*.excuse)|*.excuse|All files (*.*)|*.*";
                    fileDialog.FileName = descriptionTextBox.Text + ".excuse";
                    DialogResult result = fileDialog.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        currentExcuse = new Excuse(fileDialog.FileName);
                        UpdateForm(false);
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Excuse file is incorrect or not existing.");
                }
            }
        }

        private bool CheckChanged()
        {
            if (formChanged)
            {
                DialogResult result = MessageBox.Show("Current excuse has not been saved. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                    return false;
            }
            return true;
        }

        private void randomExcuseButton_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                try
                {
                    currentExcuse = new Excuse(random, selectedFolder);
                    UpdateForm(false);
                }
                catch(IndexOutOfRangeException ex)
                {
                    MessageBox.Show("Missing excuse files");
                }
            }
        }
    }
}
