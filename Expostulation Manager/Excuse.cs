﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Expostulation_Manager
{
    [Serializable]
    class Excuse
    {
        public Excuse()
        {
            ExcusePath = "";
        }

        public Excuse(string path)
        {
            OpenFile(path);
        }

        public Excuse(Random random, string path)
        {
            string[] files = Directory.GetFiles(path, "*.excuse");
            OpenFile(files[random.Next(files.Length)]);
        }

        public string Description { get; set; }
        public string Results { get; set; }
        public DateTime LastUseed { get; set; }
        public string ExcusePath { get; set; }

        private void OpenFile(string path)
        {
            this.ExcusePath = path;
            BinaryFormatter formateer = new BinaryFormatter();
            Excuse tempExcuse;
            using (Stream input = File.OpenRead(ExcusePath))
            tempExcuse = (Excuse)formateer.Deserialize(input);
            Description = tempExcuse.Description;
            Results = tempExcuse.Results;
            LastUseed = tempExcuse.LastUseed;
        }

        public void Save(string fileName)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (Stream output = File.OpenWrite(fileName))
                formatter.Serialize(output, this);
        }
    }
}
